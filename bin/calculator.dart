class Calculator{
  Calculator();

  double add(double input1, double input2) {
    return input1 + input2;
  }

  double substract(double input1,double input2) {
    return input1 - input2;
  }

  double multiple(double input1, double input2) {
    return input1 * input2;
  }

  double divide(double input1, double input2) {
    if (input2 == 0) {
      return 0;
    } else {
      return input1 / input2;
    }
  }

  double modulo(double input1, double input2) {
    return input1 % input2;
  }

  double exponent(double input1) {
    return input1 * input1;
  }
}
void main() {
  Calculator myCal = Calculator();
  print('Result of Add is :');
  print(myCal.add(2, 3));
  print('Result of Substract is :');
  print(myCal.substract(6, 2));
  print('Result of Multiple is :');
  print(myCal.multiple(4, 4));
  print('Result of Divide is :');
  print(myCal.divide(1, 0));
  print('Result of Modulo is :');
  print(myCal.modulo(2, 5));
  print('Result of Exponent is :');
  print(myCal.exponent(5));
  
}
